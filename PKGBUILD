# Maintainer: Sven-Hendrik Haase <svenstaro@archlinux.org>
# Contributor: Julius Michaelis <gitter@liftm.de>

pkgname=wasmer
pkgver=5.0.4
pkgrel=1
pkgdesc="Universal Binaries Powered by WebAssembly"
arch=('x86_64')
url="https://github.com/wasmerio/wasmer"
license=('MIT')
depends=('gcc-libs' 'zlib' 'ncurses' 'libffi' 'libxkbcommon')
makedepends=('rustup' 'cmake')
source=("$pkgname-$pkgver.tar.gz::https://github.com/wasmerio/wasmer/archive/refs/tags/v${pkgver}.tar.gz"
        reproducible-builds.patch)
sha512sums=('e41dba227d105a4dc6e9c30b4952e0b68ceaea30ec8af0706247ba0868d6ebed70609d1c32114a7793c5d92f66c03818f93abf92b1cc4124940b51c6adfb9a50'
            'dbefb95fb2cb4449251451e931f6c3ee2e128809b34f096393b5dbf9d260a55185e5542aafce2714d8db9baded086ec9af97b63d87340d8a22d3010a1f756a17')
options=('staticlibs' '!lto')

prepare() {
  cd "$pkgname-$pkgver"
  patch -Np1 -i "$srcdir/reproducible-builds.patch"
}

build() {
  cd "$pkgname-$pkgver"
  # Activate LLVM once
  # https://github.com/TheDan64/inkwell/issues/406 lands.
  make ENABLE_CRANELIFT=1 ENABLE_LLVM=0 WASMER_INSTALL_PREFIX=/usr
}

check() {
  cd "$pkgname-$pkgver"
  make test-examples
}

package() {
  cd "$pkgname-$pkgver"
  make DESTDIR="$pkgdir"/usr install
}

# vim:set ts=2 sw=2 et:
